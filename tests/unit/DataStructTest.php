<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datastruct\DatastructTest;

use Spinit\Datastruct\DataStruct;
use Spinit\Datastruct\Field;
use Spinit\Datastruct\Index;
use Spinit\Util\Dictionary;

use PHPUnit\Framework\TestCase;

/**
 * Description of DataStructTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class DataStructTest extends TestCase
{
    private $object;
    
    public function setUp() {
        parent::setUp();
        $this->object = new DataStruct('prova');
    }
    
    public function testField()
    {
        $this->assertCount(0, $this->object->getFieldList());
        $this->object->addField(new Field('test_field'));
        $this->assertCount(1, $this->object->getFieldList());
        $item = $this->object->getField('test_field');
        $this->assertEquals('test_field', $item->getName());
        $this->assertEquals($this->object, $item->getDataStruct());
    }
    
    public function testParam()
    {
        $this->object->setParam('param1', 'value1');
        $this->assertEquals('value1', $this->object->getParam('param1'));
    }
    public function testIndex()
    {
        $this->assertCount(0, $this->object->getIndexList());
        $this->object->addIndex(new Index('testIndex'));
        $this->assertCount(1, $this->object->getIndexList());
        $item = $this->object->getIndex('testIndex');
        $this->assertEquals('testIndex', $item->getName());
        $this->assertEquals($this->object, $item->getDataStruct());
        
        $this->assertCount(0, $item->getFieldList());
        $item->addField('test_field_index');
        $this->assertCount(1, $item->getFieldList());
        
        $this->assertTrue($item->hasField('test_field_index'));
    }
    
    public function testDiffField()
    {
        $s1 = new DataStruct('p1');
        $s2 = new DataStruct('p1');
        
        $s1->addField(new Field('f1'))->set('type','string')->set('size','10');
        
        $s2->addField(new Field('f1'))->set('type','string')->set('size','50');
        $s2->addField(new Field('f2'))->set('type','string')->set('size','50');
        
        $dic = new Dictionary();
        $ar = new \ArrayObject();
        $dic->bindExec('insertField', function ($event) use ($ar) {
           $ar['insert'] = $event->getParam(0)->getName();
        });
        $dic->bindExec('updateField', function ($event) use ($ar) {
           $ar['update'] = $event->getParam(0)->getName();
        });
        $s1->execDiff($s2, $dic);
        
        $this->assertEquals('f2', $ar['insert']);
        $this->assertEquals('f1', $ar['update']);
    }
    
    public function testDiffIndex()
    {
        $s1 = new DataStruct('p1');
        $s2 = new DataStruct('p1');
        
        $i1 = $s1->addIndex(new Index('i1'));
        $i1->addField('f1');
        $i1->addField('f3');
        
        $i2 = $s2->addIndex(new Index('i1'));
        $i2->addField('f1');
        $i2->addField('f2');
        
        $i3 = $s2->addIndex(new Index('i3'));
        $i3->addField('f1');
        
        $dic = new Dictionary();
        $dic->bindExec('insertIndex', function ($event) use ($dic) {
           $dic['insert'] = $event->getParam(0)->getName();
        });
        $dic->bindExec('updateIndex', function ($event) use ($dic) {
           $dic['update'] = $event->getParam(0)->getName();
        });
        $s1->execDiff($s2, $dic);

        $this->assertEquals('i3', $dic['insert']);
        $this->assertEquals('i1', $dic['update']);
    }
}
