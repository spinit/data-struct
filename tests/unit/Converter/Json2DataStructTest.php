<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datastruct\Converter\Json2DatastructTest;

use PHPUnit\Framework\TestCase;
use Spinit\Datastruct\Converter\Json2DataStruct;
use Spinit\Datastruct\Converter\Xml2DataStruct;
use Spinit\Util;

/**
 * Description of Json2DataStructTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Json2DataStructTest extends TestCase
{
    /**
     *
     * @var Json2DataStruct
     */
    private $object;
    public function setUp()
    {
        $this->object = new Json2DataStruct(Util\file_get_contents(__DIR__.'/Test-A1.json'));
    }
    public function testData()
    {
        $this->assertEquals('test', $this->object->getDataStruct('test')->getName());
    }
    public function testDiffJson()
    {
        $object2 = new Json2DataStruct(Util\file_get_contents(__DIR__.'/Test-A2.json'));
        
        $t1 = $this->object->getDataStruct('test');
        $t2 = $object2->getDataStruct('test');
        $this->analizeStruct($t1, $t2);
    }
    public function testDiffXml()
    {
        $object1 = new Xml2DataStruct(Util\file_get_contents(__DIR__.'/Test-A1.xml'));
        $object2 = new Xml2DataStruct(Util\file_get_contents(__DIR__.'/Test-A2.xml'));
        $t1 = $object1->getDataStruct('test');
        $t2 = $object2->getDataStruct('test');
        $this->analizeStruct($t1, $t2);
    }
    
    private function analizeStruct($t1, $t2)
    {
        $opr = [];
        // preparazione listener
        $t1->bindExec('insertField', function($event) use (&$opr) {
            $field = $event->getParam(0);
            $opr['insert'][] = implode(':', [$field->getName(), $field->get('type'), $field->get('size')]);
        });
        $t1->bindExec('updateField', function($event) use (&$opr) {
            $field = $event->getParam(0);
            $found = $event->getParam(1);
            $opr['update'][] = implode(':', [$field->getName(), $field->get('type'), $field->get('size'), $found->get('type'), $found->get('size')]);
        });
        // esecuzione
        $t1->execDiff($t2);
        
        // analisi risultati
        $this->assertEquals(1, count($opr['insert']));
        $this->assertEquals(1, count($opr['update']));
        
        $this->assertEquals('k2:varchar:50', $opr['insert'][0]);
        $this->assertEquals('k1:varchar:50:varchar:60', $opr['update'][0]);
    }
}
