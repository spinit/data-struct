<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datastruct\Converter\MysqldumpXml2DataStructTest;

use PHPUnit\Framework\TestCase;
use Spinit\Datastruct\Converter\MysqldumpXml2DataStruct;
use Spinit\Util;

/**
 * Description of Json2DataStructTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class MysqldumpXml2DataStructTest extends TestCase
{
    /**
     *
     * @var MysqldumpXml2DataStruct
     */
    private $object;
    public function setUp()
    {
        $this->object = new MysqldumpXml2DataStruct(__DIR__.'/Test-D1.xml');
    }
    public function xtestData()
    {
        $ds = $this->object->getDataStruct('osy_user');
        $this->assertEquals('osy_user', $ds->getName());
        $field = $ds->getField('nam');
        $this->assertEquals('100', $field->get('size'));
        $this->assertEquals('varchar', $field->get('type'));
    }
    public function testType()
    {
        $ds = $this->object->getDataStruct('osy_user_group');
        $field = $ds->getField('id');
        $this->assertEquals('int', $field->get('type'));
        $this->assertEquals('11', $field->get('size'));
        $this->assertEquals('43', $field->get('incval'));
        
        $field2 = $ds->getField('uid');
        $this->assertEquals('', $field2->get('size'));
        $this->assertEquals('uuid', $field2->get('type'));
        
        $ds = $this->object->getDataStruct('osy_user_test');
        $field = $ds->getField('id');
        $this->assertEquals('increment', $field->get('type'));
        $this->assertEquals('', $field->get('size'));
        $this->assertEquals('43', $field->get('incval'));
    }
    public function testList()
    {
        $list = $this->object->getDataStructList();
        $this->assertArrayHasKey('osy_user', $list);
    }
}
