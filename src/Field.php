<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datastruct;

use Spinit\Util;
use Spinit\Util\Error\NotFoundException;

/**
 * Description of Field
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class Field
{
    private $data = [];
    private $dataStruct = null;
    
    public function __construct($name, $data = [])
    {
        $this->set('name', $name);
        foreach($data ?: [] as $k=>$v) {
            $this->set($k, $v);
        }
    }
    public function getData()
    {
        return $this->data;
    }
    public function getName()
    {
        return strtolower($this->get('name','?'));
    }
    
    public function setDataStruct(DataStruct $dataStruct)
    {
        $this->dataStruct = $dataStruct;
    }
    
    public function getDataStruct()
    {
        return $this->dataStruct;
    }
    
    /**
     * Se non viene trovato il campo richiesto allora viene sollevata un'eccezione a meno che non viene indicato 
     * un secondo parametro che rappresenta il default
     * @param type $name
     * @return type
     */
    public function get($name)
    {
        $args = func_get_args();
        if (count($args) < 2) {
            $resName = ($this->dataStruct ? $this->dataStruct->getName() : '--').'.'.$this->getName().':'.$name;
            return Util\arrayGetAssert($this->data, $name, 'Property not found : '. $resName);
        }
        return Util\arrayGet($this->data, $name, $args[1]);
    }
    
    public function set($name, $value)
    {
        $this->data[$name] = $value;
        return $this;
    }
    
    public function isSame(Field $field)
    {
        foreach($field->getData() as $k => $v) {
            if (Util\arrayGet($this->data, $k).'' != $v.'') {
                return false;
            }
        }
        return true;
    }
    
    public function clear()
    {
        $this->data = [];
        return $this;
    }
}
