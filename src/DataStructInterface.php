<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datastruct;

use Spinit\Util\TriggerInterface;

/**
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface DataStructInterface
{
    public function getName();
    public function getParam($name);
    public function getPkey();
    public function addPkeyField($fieldName);
    public function getField($name);
    public function getFieldList();
    public function getIndex($name);
    public function getIndexList();
    public function execDiff(DataStructInterface $otherStruct, TriggerInterface $listener = null);
}
