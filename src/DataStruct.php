<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datastruct;

use Spinit\Util;
use Spinit\Util\Error\NotFoundException;

use Spinit\Util\TriggerInterface;

/**
 * Description of DataStruct
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataStruct implements DataStructInterface, TriggerInterface
{
    private $name = '';
    private $fields = [];
    private $index = [];
    private $pkey = null;
    private $param = [];
    
    use Util\TriggerTrait;
    
    public function __construct($name)
    {
        $this->setName($name);
        $this->pkey = new Index('');
    }
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getParam($name)
    {
        return Util\arrayGet($this->param, $name);
    }
    public function setParam($name, $value)
    {
        $this->param[$name] = $value;
        return $this;
    }

    /**
     * restituisce la lista dei campi che formano la primary key
     * @return type
     */
    public function getPkey()
    {
        return $this->pkey->getFieldList();
    }
    
    /**
     * Aggiunge un campo alla primary key
     * @param type $field
     * @return $this
     */
    public function addPkeyField($field)
    {
        $this->pkey->addField($field);
        return $this;
    }
    
    /**
     * Aggiunge un campo alla primary key
     * @param type $field
     * @return $this
     */
    public function removePkeyField($field)
    {
        $this->pkey->removeField($field);
        return $this;
    }
    public function addField(Field $field)
    {
        $this->fields[$field->getName()] = $field;
        $field->setDataStruct($this);
        return $field;
    }
    
    public function addIndex(Index $index)
    {
        $this->index[$index->getName()] = $index;
        $index->setDataStruct($this);
        return $index;
    }
    
    public function getField($name)
    {
        return Util\arrayGetAssert($this->fields, $name, 'Field not found : '.$name);
    }
    public function getFieldList()
    {
        return $this->fields;
    }
    
    public function getIndex($name)
    {
        return Util\arrayGetAssert($this->index, $name, 'Index not found : '.$name);
    }
    public function getIndexList()
    {
        return $this->index;
    }
    
    /**
     * Analizza la struttura e genera gli eventi sulle differenze riscontrate
     * @param \Spinit\Datastruct\DataStructInterface $struct
     * @param TriggerInterface $listener
     */
    public function execDiff(DataStructInterface $struct, TriggerInterface $listener = null)
    {
        $executor = $listener ?: $this;
        // si analizzano i campi della struttura passata per vedere se occorre cambiare la struttura attuale
        foreach($struct->getFieldList() as $name => $item) {
            try {
                $found = $this->getField($name);
                $found->isSame($item) ? '' : $executor->trigger('updateField', [$found, $item]);
            } catch (NotFoundException $ex) {
                $executor->trigger('insertField', [$item]);
            }
        }
        foreach($struct->getIndexList() as $name => $item) {
            try {
                $found = $this->getIndex($name);
                $found->isSame($item) ? '' : $executor->trigger('updateIndex', [$found, $item]);
            } catch (NotFoundException $ex) {
                // non c'è ...
                $executor->trigger('insertIndex', [$item]);
            }
        }
    }
}
