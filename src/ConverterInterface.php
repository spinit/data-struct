<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datastruct;

/**
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface ConverterInterface
{
    public function getResourceList();
    public function getDataStruct($table);
    public function getDataStructList();
}
