<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datastruct;

use Spinit\Util;
use Webmozart\Assert\Assert;

/**
 * Description of Index
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Index
{
    private $name = [];
    private $dataStruct = null;
    private $fields = [];
    private $type;
    
    public function __construct($name, $type = '')
    {
        $this->name = $name;
        $this->type = $type;
    }
    
    public function getType()
    {
        return $this->type;
    }
    public function getName()
    {
        return $this->name;
    }
    
    public function setDataStruct(DataStruct $dataStruct)
    {
        $this->dataStruct = $dataStruct;
    }
    
    public function getDataStruct()
    {
        return $this->dataStruct;
    }
    
    public function addField($field)
    {
        if (!$field) {
            return;
        }
        if (is_array($field)) {
            foreach($field as $item) {
                $this->addField($item);
            }
        } else {
            $this->fields[] = $field;
        }
        return $this;
    }
    
    public function removeField($field)
    {
        $this->fields = array_diff($this->fields, [$field]);
        return $this;
    }
    public function getFieldList()
    {
        return $this->fields;
    }
    
    public function hasField($name)
    {
        return in_array($name, $this->fields);
    }
    
    public function isSame(Index $index)
    {
        return $this->fields == $index->getFieldList();
    }
}
