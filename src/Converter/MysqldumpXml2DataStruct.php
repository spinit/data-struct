<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datastruct\Converter;

use Spinit\Util;
use Spinit\Util\Error\NotFoundException;

/**
 * Description of PDO2DataStruct
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class MysqldumpXml2DataStruct extends Json2DataStruct
{
    private $arData = [];
    private $table = [];
    public function __construct($dataFile)
    {
        $handle = false;
        try {
            $handle = fopen($dataFile, "r");
        } catch (\Exception $ex) {
            throw new NotFoundException('File not found : '.$dataFile);
        }
        // open xml file
        if ($handle) {
            // Creates a new XML parser and returns a resource handle referencing it to be used by the other XML functions. 
            $parser = xml_parser_create(); 

            xml_set_element_handler($parser, array($this, "tagStartElements"), array($this, "tagEndElements"));
            xml_set_character_data_handler($parser, array($this, "tagCharacterData"));

            while($data = fread($handle, 4096)) { // read xml file
               xml_parse($parser, $data);  // start parsing an xml document 
            }
            fclose($handle);
            xml_parser_free($parser); // deletes the parser
        }

        
        parent::__construct($this->arData);
    }
    
    // Called to this function when tags are opened 
    private function tagStartElements($parser, $name, $attr)
    {
        switch($name) {
            case 'TABLE_STRUCTURE':
                $this->initTable($attr);
                break;
            case 'FIELD':
                $this->addField($attr);
                break;
            case 'KEY':
                $this->addIndex($attr);
                break;
            case 'OPTIONS':
                $this->addOption($attr);
                break;
        }
    }

    // Called to this function when tags are closed 
    private function tagEndElements($parser, $name)
    {
        switch($name) {
            case 'TABLE_STRUCTURE':
                $this->endTable();
                break;
        }
    }

    // Called on the text between the start and end of the tags
    private function tagCharacterData($parser, $data) {
    }

    private function initTable($attr)
    {
        $this->table = [
            'info' => $attr,
            'option'=>[],
            'autoinc' => '',
            'data'=> [
                'field'=>[],
                'index'=>[]
            ]
        ];
    }

    private function addOption($attr)
    {
        $this->table['option'] = $attr;
        if ($this->table['autoinc']) {
            $field = Util\arrayGetAssert($this->table['data']['field'], $this->table['autoinc']);
            $this->table['data']['field'][$this->table['autoinc']]['incval'] = $attr['AUTO_INCREMENT'];
        }
    }
    
    private function endTable()
    {
        $this->arData[$this->table['info']['NAME']] = $this->table['data'];
    }
    
    private function addField($attr)
    {
        @preg_match_all("/(\w+)(\((\w+)\))?( (\w+))?/", $attr['TYPE'], $LVar, PREG_PATTERN_ORDER);
        $tt = [$LVar[1][0], $LVar[3][0], $LVar[5][0]];
        $notnull = (Util\arrayGet($attr, 'NULL') == 'YES' ? false : true);
        $default = Util\arrayGet($attr, 'DEFAULT', null);
        switch($tt[0]) {
            case 'binary':
                if ($tt[1] == '16') {
                    $tt = ['uuid', '', ''];
                }
                break;
            case 'bigint':
                if ($tt[2] == 'unsigned' and Util\arrayGet($attr, 'EXTRA') == 'auto_increment') {
                    $tt = ['increment', '', ''];
                }
                break;
        }
        if (Util\arrayGet($attr, 'EXTRA') == 'auto_increment') {
            // viene memorizzato il nome del campo che ha l'auto increment
            $this->table['autoinc'] = Util\arrayGet($attr, 'FIELD', '');
        }
        $this->table['data']['field'][Util\arrayGet($attr, 'FIELD')] = [
            'type'=>$tt[0],
            'size'=>$tt[1],
            'notnull'=>$notnull,
            'default'=>$default,
            'incval'=>'',
            'autoinc' => Util\arrayGet($attr, 'EXTRA') == 'auto_increment',
            'unsigned' => $tt[2] == 'unsigned',
            'ispkey'=> Util\arrayGet($attr, 'KEY') == 'PRI' ? true : false,
        ];
    }
    
    private function addIndex($attr)
    {
        Util\arrayGet($this->table['data']['index'], $attr['KEY_NAME'], function () use ($attr) {
            return $this->table['data']['index'][$attr['KEY_NAME']] = ['type' => $attr['INDEX_TYPE'], 'field' => []];
        });
        $this->table['data']['index'][$attr['KEY_NAME']]['field'][] = $attr['COLUMN_NAME'];
    }
}
