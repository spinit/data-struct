<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datastruct\Converter;

use Spinit\Datastruct\ConverterInterface;
use Spinit\Datastruct\DataStruct;
use Spinit\Datastruct\Field;
use Spinit\Datastruct\Index;
use Spinit\Util;
use Webmozart\Assert\Assert;

/**
 * Description of PDO2DataStruct
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Json2DataStruct implements ConverterInterface
{
    public function __construct($data)
    {
        if (is_array($data)) {
            $this->data = $data;
        } else {
            $this->data = json_decode($data, 1);
        }
    }
    
    public function getResourceList()
    {
        return array_keys($this->data);
    }
    
    public function getDataStruct($table)
    {
        Assert::keyExists($this->data, $table, 'Tabella non presente : '.$table);
        $ds = new DataStruct($table);
        foreach(Util\arrayGet($this->data[$table], 'field', []) as $name => $conf) {
            $field = $ds->addField(new Field($name))
                        ->set('type', Util\arrayGet($conf, 'type'))
                        ->set('size', Util\arrayGet($conf, 'size'))
                        ->set('notnull', Util\arrayGet($conf, 'notnull'))
                        ->set('incval', Util\arrayGet($conf, 'incval'))
                        ->set('autoinc', Util\arrayGet($conf, 'autoinc', false))
                        ->set('unsigned', Util\arrayGet($conf, 'unsigned', false))
                        ->set('ispkey', Util\arrayGet($conf, 'ispkey', false))
                        ->set('default', Util\arrayGet($conf, 'default'));
            if (Util\arrayGet($conf, 'ispkey')) {
                $ds->addPkeyField($name);
            }
        }
        foreach(Util\arrayGet($this->data[$table], 'index', []) as $name => $conf) {
            $ds->addIndex(new Index($name, Util\arrayGet($conf, 'type')))->addField(Util\arrayGet($conf, 'field'));
        }
        return $ds;
    }
    
    public function getDataStructList()
    {
        $list = [];
        foreach($this->getResourceList() as $resource) {
            $list[$resource] = $this->getDataStruct($resource);
        }
        return $list;
    }
}
