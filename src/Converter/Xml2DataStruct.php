<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Datastruct\Converter;

use Spinit\Util;

/**
 * Description of PDO2DataStruct
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Xml2DataStruct extends Json2DataStruct
{
    public function __construct($dataString)
    {
        parent::__construct(json_encode(simplexml_load_string($dataString)));
    }
}
